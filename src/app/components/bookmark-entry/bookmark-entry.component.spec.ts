import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupComponent } from './group.component';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import { Bookmark } from '../../models/bookmark.model';
import { initialState } from '../../store/state/app.state';
import { InitialBookmarkState } from '../../store/state/bookmark.state';

describe('GroupComponent', () => {
  let component: GroupComponent;
  let fixture: ComponentFixture<GroupComponent>;

  let store : MockStore;
  let initialBookmarks = InitialBookmarkState
  initialBookmarks.bookmarks = [{
  	id: 1,
  	title: 'angular',
  	url: 'angular.io',
  	group: 'work'
  }]

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupComponent ],
      providers: [provideMockStore({ initialState })]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return active group', () => {
  	//should return all bookmarks group on first load
  	expect(component.isActive('All Bookmarks')).toBeTruthy();
  	
  	component.activeGroup = 'work'
  	fixture.detectChanges()
  	expect(component.isActive('work')).toBeTruthy();
  });

  it('should be All Bookmarks on initial load', () => {
  	console.log("init: " + JSON.stringify(initialState))
  	initialState.bookmarks = initialBookmarks
  	component.loadData()
  	expect(component.activeGroup).toEqual('All Bookmarks');
  });

  it('should load the current group', () => {
  	component.loadGroup('work')
  	expect(component.bookmarks[0].group).toEqual('work')
  })

});
