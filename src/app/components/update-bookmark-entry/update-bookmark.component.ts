import { Component, Inject, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { Bookmark } from '../../store/state/bookmark.state';
import { UpdateBookmark } from '../../store/actions/bookmark.actions';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { NotificationService } from '../../services/notification.service';

@Component({
    styleUrls: ['update-bookmark.component.scss'],
    templateUrl: 'update-bookmark.component.html',
})
export class UpdateBookmarkEntryComponent {
	@ViewChild('updateCloseButton', null) updateCloseButton;
	
    constructor(private store: Store<AppState>, @Inject(MAT_DIALOG_DATA) public data: any, private notificationService: NotificationService) {
        
    }

    title: FormControl;
	url: FormControl;
	group: FormControl;
	bookmark : Bookmark;

    ngOnInit() {
	    this.title = new FormControl(this.data.title, [
	        Validators.required
	    ]);

	    this.url = new FormControl(this.data.url, [
	        Validators.required
	    ]);

        this.group = new FormControl(this.data.group, [
	        Validators.required,
            Validators.maxLength(20)
	    ]);
	}

	update() {
	    if(this.title.hasError('required')) {
		    return false;
	    }

		if(this.url.hasError('required')) {
		    return false;
	    }

		if(this.group.hasError('required') || this.group.hasError('maxlength')) {
		    return false;
	    }

		let title = this.title.value;
		let url = this.url.value;
		let group = this.group.value;

	    this.bookmark = {
		    id: this.data.id, title, url, group
	    }

		this.store.dispatch(new UpdateBookmark(this.bookmark));

		this.updateCloseButton._elementRef.nativeElement.click();
		
		this.notificationService.showNotification({
			duration: 2000,
			vPos: 'top',
			hPos: 'center',
			message: 'Bookmark successfully updated!'
		});
    }
}
