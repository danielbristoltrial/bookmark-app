import { Component, Input, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { NotificationService } from '../../services/notification.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { InitialBookmarkState, Bookmarks } from '../../store/state/bookmark.state';

interface BasicTableConfig {
	columns: any[]
	displayedColumns: string[]
    tooltip: any
    data: any
    editPopupConfig: any
    deletePopupConfig: any
    deleteFunc: Function
}

@Component({
	selector: 'basic-table',
    styleUrls: ['basic-table.component.scss'],
    templateUrl: 'basic-table.component.html',
})
export class BasicTableComponent {

	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    @Input() config : BasicTableConfig;
    

    @ViewChild(MatPaginator) paginator = MatPaginator;

    ngAfterViewInit() {
        console.log("data list: " + JSON.stringify(this.config.data));
        this.config.data = new MatTableDataSource<Bookmarks>(this.config.data);
        this.config.data.paginator = this.paginator;
    }

	delete(data: any) {
		this.config.deleteFunc(data, this.store, this.notificationService);
	}

}
