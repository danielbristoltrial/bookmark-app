import {Component, ViewChild} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state';
import { Bookmark } from '../../store/state/bookmark.state';
import { AddBookmark } from '../../store/actions/bookmark.actions';
import { NotificationService } from '../../services/notification.service';

@Component({
    styleUrls: ['add-bookmark.component.scss'],
    templateUrl: 'add-bookmark.component.html',
})
export class AddBookmarkEntryComponent {
	@ViewChild('addCloseButton', null) addCloseButton;
	
    constructor(private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    title: FormControl;
	url: FormControl;
	group: FormControl;
	bookmark : Bookmark;

    ngOnInit() {
	    this.title = new FormControl('', [
	        Validators.required
	    ]);

	    this.url = new FormControl('', [
	        Validators.required
	    ]);

	    this.group = new FormControl('', [
	        Validators.required,
            Validators.maxLength(6)
	    ]);
	}

    add() {
	    if(this.title.hasError('required')) {
		    return false;
	    }

		if(this.url.hasError('required')) {
		    return false;
	    }

		if(this.group.hasError('required') || this.group.hasError('maxlength')) {
		    return false;
	    }

		let title = this.title.value;
		let url = this.url.value;
		let group = this.group.value;

	    this.bookmark = {
		    id: new Date().getTime(), title, url, group
	    }

		this.store.dispatch(new AddBookmark(this.bookmark));
		
		this.addCloseButton._elementRef.nativeElement.click();
		
		this.notificationService.showNotification({
			duration: 2000,
			vPos: 'top',
			hPos: 'center',
			message: 'Bookmark successfully added!'
		});
    }
}
