import { Component, Input, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/state/app.state'; 
import { NotificationService } from '../../services/notification.service';

interface MessageConfig {
	message: string
	primaryButtonEnabled: boolean
	primaryButtonLabel: string
	primaryButtonCallback: Function
	secondaryButtonEnabled: boolean
	secondaryButtonLabel: string
	data: any
}

interface PopupConfig {
	type: string
	attribute: string
	fontIconColor: string
	fontIcon: string
	tooltip: string
	template: any
	messageConfig: MessageConfig
}

@Component({
    selector: 'popup',
    styleUrls: ['popup.component.scss'],
    templateUrl: 'popup.component.html',
})
export class PopupComponent {
    constructor(public dialog: MatDialog, private store: Store<AppState>, private notificationService: NotificationService) {
        
    }

    @Input() config : PopupConfig;
    @Input() data : any;

    openDialog() {
        this.dialog.open(this.config.template, {data: this.data});
    }

    openMessageDialog() {
	    this.config.messageConfig.data=this.data;
	
	    const dialogRef = this.dialog.open(MessageComponent,  {
            width: '250px',
            data: this.config.messageConfig
        });

        dialogRef.afterClosed().subscribe(data => {
            if(data) {
	            this.config.messageConfig.primaryButtonCallback(this.store, this.notificationService, data)
            }
        });
    }
}

@Component({
  templateUrl: './message.component.html'
})
export class MessageComponent {
	
	constructor(public dialogRef: MatDialogRef<MessageComponent>, @Inject(MAT_DIALOG_DATA) public config: MessageConfig) {
        
    }

    onSecondaryClick(): void {
        this.dialogRef.close();
    }
}