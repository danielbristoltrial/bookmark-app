import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatTreeModule } from '@angular/material/tree';
import { OverlayModule } from '@angular/cdk/overlay';
import {MatPaginatorModule} from '@angular/material/paginator';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { initialState, reducers, effects } from './store/state/app.state';

import { FlexLayoutModule } from '@angular/flex-layout';

import { BookmarkEntryComponent } from './components/bookmark-entry/bookmark-entry.component';
import { PopupComponent } from './components/popup/popup.component';
import { MessageComponent } from './components/popup/popup.component';
import { BasicTableComponent } from './components/basic-table/basic-table.component';
import { AddBookmarkEntryComponent } from './components/add-bookmark-entry/add-bookmark.component';
import { UpdateBookmarkEntryComponent } from './components/update-bookmark-entry/update-bookmark.component';

import { BookmarkService } from './services/bookmark.service';
import { NotificationService } from './services/notification.service';
import { BookmarkHeaderComponent } from './components/bookmark-header/bookmark-header.component';

@NgModule({
    imports: [
        BrowserModule,
        ReactiveFormsModule,
        HttpClientModule,
        AppRoutingModule,
        BrowserAnimationsModule,
	    MatButtonModule,
	    MatDialogModule,
	    MatIconModule,
	    MatInputModule,
	    MatSidenavModule,
	    MatSnackBarModule,
	    MatSortModule,
	    MatTableModule,
	    MatToolbarModule,
	    MatTooltipModule,
	    OverlayModule,
        FormsModule,
        FlexLayoutModule,
        MatPaginatorModule,
        EffectsModule.forRoot(effects),
        StoreModule.forRoot(reducers, {initialState}),
        StoreDevtoolsModule.instrument( {maxAge: 30} ),
    ],
    declarations: [
        AppComponent,
        BookmarkEntryComponent,
        PopupComponent,
        MessageComponent,
        BasicTableComponent,
        AddBookmarkEntryComponent,
        UpdateBookmarkEntryComponent,
        BookmarkHeaderComponent
    ],
    providers: [
	    BookmarkService, NotificationService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }