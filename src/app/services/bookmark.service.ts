import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Bookmarks } from '../store/state/bookmark.state';
import {Bookmark} from '../models/bookmark.model';


@Injectable({providedIn: 'root'})
export class BookmarkService {

    private initialBookmarks: Bookmarks = {
        bookmarks: [
            {id:1, title: 'Bootstrap', url: 'https://getbootstrap.com', group: 'work'},
            {id:2, title: 'Facebook', url: 'https://facebook.com', group: 'leisure'},
            {id:3, title: 'Youtube', url: 'https://youtube.com', group: 'personal'},
            {id:4, title: 'Angular', url: 'https://angular.io', group: 'work'},
            {id:5, title: 'Google', url: 'https://google.com', group: 'leisure'},
            {id:6, title: 'Material Icons', url: 'https://fonts.google.com/icons?selected=Material+Icons', group: 'work'}
        ]
    };

    getBookmarks(): Observable<Bookmarks>{
        return new Observable(observer => {
            observer.next(this.initialBookmarks);
            observer.complete();
        });
    }
}
